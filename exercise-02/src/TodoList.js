export default function TodoList({items}) {
    if (items && items.length > 0){
        return(
            <ul>
                {items.map((todo, index)=> <li key={index}>{todo}</li>)}
            </ul>
        )
    }else{
        return (
            <p>There are no to-do items!</p>
        )
    }

}