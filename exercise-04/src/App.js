import { useState } from 'react';
import NewToDoItem from './NewToDoItem';
import ToDoList from './ToDoList';

// A to-do list to use for testing purposes
const initialTodos = [
    { description: 'Finish lecture', isComplete: true },
    { description: 'Do homework', isComplete: false },
    { description: 'Sleep', isComplete: true }
];

function App() {

    const [todos, setTodos] = useState(initialTodos);

    function handleTodoStatusChanged(index, isComplete) {
        const newTodos = [...todos];
        newTodos[index] = { ...todos[index], isComplete };
        setTodos(newTodos);
    }

    function handleAddTodo(description) {
        setTodos([
            ...todos,
            {
                description,
                isComplete: false
            }
        ]);
    }

    function handleRemoveTodo(index) {
        const newTodos = [...todos];
        newTodos.splics(index, 1);
        setTodos(newTodos);
    }

    return (
        <div>
            <div className="box">
                <h1>My todos</h1>
                <ToDoList items={todos}
                          onTodoStatusChanged={handleTodoStatusChanged}
                          onRemove={handleRemoveTodo} />
            </div>
            <div className="box">
                <h1>Add item</h1>
                <NewToDoItem onAddTodo={handleAddTodo} />
            </div>

        </div>
    );
}

export default App;