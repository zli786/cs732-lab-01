import styles from './ToDoList.module.css';

export default function ToDoList({items, onTodoStatusChanged, onRemove}) {
    if (items && items.length > 0) {
        return items.map((todo, index) =>
            <div key={index} className={styles.todo}>
                <label className={todo.isComplete ? styles.done : undefined}>
                    <input type="checkbox"
                           checked={todo.isComplete}
                           onChange={e => onTodoStatusChanged(index, e.target.checked)} />
                    {todo.description}
                    {todo.isComplete && <span> (Done!)</span>}
                </label>
                <button onClick={() => onRemove(index)}>Remove</button>
            </div>);
    } else {
        return (
            <p>There is no to-do items!</p>
        )

    }


}
